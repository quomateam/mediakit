<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Los Andes Mediakit</title>

	<!--BOOTSTRAP-->
	 <link href="css/bootstrap.min.css" rel="stylesheet">

	<!--MEDIAKIT CSS-->
	<link href="css/mediakit_css/styles.css" rel="stylesheet">

	<!-- GOOGLE FONTS-->
	<!-- MADA -->
	<link href="https://fonts.googleapis.com/css?family=Mada:400,600,700" rel="stylesheet">

	<!-- FAVICON -->
	<link rel="icon" type="image/png" href="images/favicon.ico">

	
</head>
<body class="bg-gray">
	<!-- NAVBAR -->
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	  <div class="container">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="index.php"><img src="images/losandes.png" alt="Los Andes"></a>
	    </div>
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li><a href="index.php">Inicio</a></li>
		    <li class="dropdown">
	          <a href="formatos_tradicionales.php" class="dropdown-toggle" data-toggle="dropdown">Formatos<span class="caret"></span></a>
	          <ul class="dropdown-menu bg-blue" role="menu">
	            <li><a href="formatos_tradicionales.php">Tradicionales</a></li>
			    <li><a href="formatos_publinota.php">Publinota</a></li>
			    <li><a href="formatos_social.php">Mailing y Social Media</a></li>
			    <li><a href="formatos_mobile.php">Mobile</a></li>
				<li><a href="formatos_app.php">App's</a></li>
	          </ul>
	        </li>
	        <li><a href="contacto.php">Contacto</a></li>
	      </ul>	     
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container -->
	</nav>

	
	<div class="container-fluid">
		<div class="row">

			<!-- JUMBOTRON -->
			<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
				<div class="jumbotron center-block">	
				    <img src="images/hero.png" class="img-responsive" alt="Mediakit">
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-lg-12">
				<!-- INDEX HEADER -->
				<div class="col-lg-12 paddingAll20">
					<h1 class="text-center font-pt-bold text-blue font-header noMargin">Mediakit para Anunciantes</h1>
					<h4 class="text-center font-pt-bold text-blue font-subheader">-2017-</h4>
				</div>
				<div class="col-lg-1 col-md-1 hidden-sm hidden-xs bg-grayd absolute text-center text-white">
					<h4 class="font-pt-bold">Bajar para Seguir Leyendo</h4>
					<img src="images/arrow.png" />
				</div>
			</div>
		</div>
	

		<div class="row">
			<div class="col-lg-12 bg-grayl">
				<!-- METRICAS -->
				<h1 class="font-pt-bold text-blue font-header  paddingAll30 paddingLeft50">Métricas</h1>
			</div>
			<div class="col-lg-12">
				<div class="col-lg-5 col-lg-offset-2 col-md-7 col-sm-7 col-xs-12 bg-blue text-white paddingLeft50 marginTop60 index-box">
					<!-- PAGINAS VISTAS-->
					<h3 class="font-pt-regular font-header noMargin marginTop60">Páginas Vistas</h3>
					<h1 class="font-pt-bold font-giant marginBottom60">60.616.146</h1>
					<!-- USUARIOS UNICOS -->
					<h3 class="font-pt-regular font-header noMargin">Navegadores Únicos</h3>
					<h1 class="font-pt-bold font-giant marginBottom60">5.245.365</h1>
					<!-- USUARIOS -->
					<h3 class="font-pt-regular font-header noMargin">Visitas</h3>
					<h1 class="font-pt-bold font-giant marginBottom60">12.628.113</h1>
				</div>
				<div class="col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1 col-xs-12 index-box">
					<div class="col-lg-12 marginBottom60 marginTop60">
						<!-- PENETRACION MOVIL-->
						<h3 class="font-pt-regular font-subheader text-gd">Penetración Móvil</h3>
						<h1 class="font-pt-bold font-header">73%</h1>
					</div>
					<!-- TOTAL MOVIL-->
					<div class="col-lg-12 marginBottom60">
						<h3 class="font-pt-regular font-subheader text-gd">Total Páginas Vistas Móvil</h3>			
						<h1 class="font-pt-bold font-header">
						31.336.149</h1>
					</div>					
					<!-- TRAFICO EN REDES -->
					<div class="col-lg-12 marginBottom60">
						<h3 class="font-pt-regular font-subheader text-gd">Tráfico en Redes</h3>			
						<h1 class="font-pt-bold font-header">1.835.877<br>(35%)</h1>
					</div>
					<!-- TIEMPO MEDIA -->
					<div class="col-lg-12 marginBottom60">
						<h3 class="font-pt-regular font-subheader text-gd">Tiempo Media</h3>			
						<span class="font-pt-bold font-header">00:12:39 <span class="font-span">min/seg</span></span>
					</div>				
				</div>			
			</div>	
			<div class="clearfix"></div>
			<!-- REDES SOCIALES -->
		  	<div class="col-lg-12 paddingAll50 study-box social-box">
		  		<div class="col-lg-8 col-lg-offset-2">
			  		<h3 class="font-pt-regular text-gd font-title marginBottom20">Redes Sociales</h3>
			  		<!-- FACEBOOK -->
			  		<div class="col-md-4 col-sm-6 col-xs-6">
			  			<div class="col-lg-9 text-right">
			  				<h1 class="font-pt-bold font-header noMargin">+800 mil</h1>
			  			</div>
			  			<div class="col-lg-3">
			  				<h3 class="font-pt-bold text-gd font-title"><img src="images/facebook.png"></h3>
			  			</div>
			  		</div>
					<!-- TWITTER -->
			  		<div class="col-md-4 col-sm-6 col-xs-6">
			  			<div class="col-lg-9 text-right">
			  				<h1 class="font-pt-bold font-header noMargin">+185 mil</h1>
			  			</div>
			  			<div class="col-lg-3">
			  				<h3 class="font-pt-bold text-gd font-title"><img src="images/twitter.png"></h3>
			  			</div>
			  		</div>
			  		<!-- INSTAGRAM -->
			  		<div class="col-md-4 col-sm-6 col-xs-6">
			  			<div class="col-lg-9 text-right">
			  				<h1 class="font-pt-bold font-header noMargin">+35 mil</h1>
			  			</div>
			  			<div class="col-lg-3">
			  				<h3 class="font-pt-bold text-gd font-title"><img src="images/instagram.png"></h3>
			  			</div>
			  		</div>
			  		
		  		</div>
		  	</div>
			<div class="clearfix"></div>
			<!-- FUENTE -->
			<div class="col-lg-3 text-center col-centered center-block paddingAll50">		
				<span class="font-pt-regular paddingAll30 ">FUENTE</span>
				<img src="images/conscore.png" alt="ConScore">
				<div class="clearfix"></div>
				<p class="font-pt-regular font-xregular marginTop20">Última Actualización: Marzo de 2015</p>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12 bg-grayl">
				<!-- PERFILES -->
				<h1 class="font-pt-bold text-blue font-header paddingAll30 paddingLeft50">Perfil del Lector</h1>
			</div>

			<div class="col-lg-10 col-lg-offset-1">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs nav-justified margintop60 " role="tablist">
				  <li class="active font-pt-regular font-title text-blue"><a href="#losandes" role="tab" data-toggle="tab">Los Andes.com.ar</a></li>
				  <li class="font-pt-regular font-title text-blue"><a href="#masdeportes" role="tab" data-toggle="tab">Más Deportes</a></li>
				  <li class="font-pt-regular font-title text-blue"><a href="#estilo" role="tab" data-toggle="tab">Estilo</a></li>
				</ul>

				<!-- Tab panels -->
				<div class="tab-content">
					<!-- LOS ANDES.COM.AR-->
				  <div class="tab-pane fade in active" id="losandes">
					  	<!-- GENERO-->
					  	<div class="col-lg-12 bg-white paddingBottom50 overflow gender-box">
					  		<h3 class="font-pt-regular text-gd font-title paddingLeft50 margintop60 marginBottom20">Género</h3>
					  		<!-- MUJER -->
					  		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					  			<div class="col-lg-3 col-lg-offset-6 col-md-3 col-md-offset-6 col-sm-5 col-sm-offset-2 col-xs-6 text-right">
					  				<img src="images/mujer.png" alt="Mujer" class="img-responsive">
					  			</div>
					  			<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
						  			<h1 class="font-pt-bold font-subjumbo">40<span class="font-span">%</span></h1>
						  			<h3 class="font-pt-bold text-g font-subheader text-left ">mujeres</h3>
					  			</div>
					  		</div>
					  		<!-- HOMBRE -->
					  		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					  			<div class="col-lg-3 col-md-3  col-sm-4 col-xs-6 text-right">
					  				<img src="images/hombre.png" alt="Hombre" class="img-responsive">
					  			</div>
					  			<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
						  			<h1 class="font-pt-bold font-subjumbo">60<span class="font-span">%</span></h1>
						  			<h3 class="font-pt-bold text-g font-subheader text-left">hombres</h3>
					  			</div>
					  		</div>
					  	</div>
					  	<!-- NSE -->
					  	<div class="col-lg-12 bg-grayl paddingAll50 overflow nse-box">
					  		<h3 class="font-pt-regular text-gd font-title paddingLeft50">Nivel Socio Económico</h3>
					  		<!-- ALTO -->
					  		<div class="col-lg-3 col-lg-offset-2 col-md-3 col-md-offset-2 col-sm-3 col-sm-offset-1 col-xs-12">
					  			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo noMargin">30<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6  col-md-6 col-sm-6 col-xs-6 ">
					  				<h3 class="font-pt-regular text-gd font-subheader">Alto</h3>
					  			</div>
					  		</div>
							<!-- MEDIO -->
					  		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
					  			<div class="col-lg-6  col-md-6 col-sm-6 col-xs-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo noMargin">66<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6  col-md-6 col-sm-6 col-xs-6">
					  				<h3 class="font-pt-regular text-gd font-subheader">Medio</h3>
					  			</div>
					  		</div>
					  		<!-- Bajo -->
					  		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
					  			<div class="col-lg-6  col-md-6 col-sm-6 col-xs-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo noMargin">4<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6  col-md-6 col-sm-6 col-xs-6">
					  				<h3 class="font-pt-regular text-gd font-subheader">Bajo</h3>
					  			</div>
					  		</div>
					  	</div>
					  
					  <!-- NIVEL DE ESTUDIOS -->
					  	<div class="col-lg-12 bg-white paddingAll50 overflow study-box">
					  		<h3 class="font-pt-regular text-gd font-title paddingLeft50 marginBottom20">Nivel de Estudios</h3>
					  		<!-- UNIVERSITARIO -->
					  		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-header noMargin">57<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-title">tiene estudios Universitarios</h3>
					  			</div>
					  		</div>
					  		<!-- TERCIARIO -->
					  		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-header noMargin">15<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-title">tiene estudios Terciarios</h3>
					  			</div>
					  		</div>
					  		<!-- SECUNDARIO -->
					  		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-header noMargin">21<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-title">tiene estudios Secundarios</h3>
					  			</div>
					  		</div>
					  		<!-- NS/NC -->
					  		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
					  			<div class="col-lg-6 text-CENTER">
					  				<h1 class="font-pt-bold font-header noMargin">7<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-title">No sabe / <br>No contesta</h3>
					  			</div>
					  		</div>
					  	</div>
					 <!-- EDAD -->
					  	<div class="col-lg-12 bg-white age-box overflow">
					  		<h3 class="font-pt-regular text-gd font-title paddingLeft50 margintop60 marginBottom20 ">Edad</h3>
					  		<!-- MENOR 24 -->
					  		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo noMargin">4<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-subheader">Menor de <br>24 años</h3>
					  			</div>
					  		</div>
							<!-- 25 Y 34 -->
					  		<div class="col-lg-4  col-md-4 col-sm-4 col-xs-4">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo noMargin">22<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-subheader">Entre <br>25 y 34 años</h3>
					  			</div>
					  		</div>
					  		<!-- 34 y 44 -->
					  		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo  noMargin">31<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-subheader">Entre <br>35 y 44 años</h3>
					  			</div>
					  		</div>
				  		</div>
				  		<div class="col-lg-12 bg-white paddingAll50 age-box overflow">
					  		<!-- 45 y 54-->
					  		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo noMargin">18<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-subheader">Entre <br>45 y 54 años</h3>
					  			</div>
					  		</div>
					  		<!-- 55 y 64 -->
					  		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo noMargin">17<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-subheader">Entre <br> 55 y 64 años</h3>
					  			</div>
					  		</div>
					  		<!-- Mas de 65 -->
					  		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo noMargin">9<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-subheader">Más de <br>65 años</h3>
					  			</div>
					  		</div>
					  	</div>
				  </div>
				  	 
				  <!-- MAS DEPORTES -->
				  <div class="tab-pane fade" id="masdeportes">
					  	<!-- GENERO-->
					  	<div class="col-lg-12 bg-white paddingBottom50 overflow gender-box">
					  		<h3 class="font-pt-regular text-gd font-title paddingLeft50 margintop60 marginBottom20">Género</h3>
					  		<!-- MUJER -->
					  		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					  			<div class="col-lg-3 col-lg-offset-6 col-md-3 col-md-offset-6 col-sm-5 col-sm-offset-2 col-xs-6 text-right">
					  				<img src="images/mujer.png" alt="Mujer" class="img-responsive">
					  			</div>
					  			<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
						  			<h1 class="font-pt-bold font-subjumbo">21<span class="font-span">%</span></h1>
						  			<h3 class="font-pt-bold text-g font-subheader text-left ">mujeres</h3>
					  			</div>
					  		</div>
					  		<!-- HOMBRE -->
					  		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					  			<div class="col-lg-3 col-md-3  col-sm-4 col-xs-6 text-right">
					  				<img src="images/hombre.png" alt="Hombre" class="img-responsive">
					  			</div>
					  			<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
						  			<h1 class="font-pt-bold font-subjumbo">79<span class="font-span">%</span></h1>
						  			<h3 class="font-pt-bold text-g font-subheader text-left">hombres</h3>
					  			</div>
					  		</div>
					  	</div>
					  	<!-- NSE -->
					  	<div class="col-lg-12 bg-grayl paddingAll50 overflow nse-box">
					  		<h3 class="font-pt-regular text-gd font-title paddingLeft50">Nivel Socio Económico</h3>
					  		<!-- ALTO -->
					  		<div class="col-lg-3 col-lg-offset-2 col-md-3 col-md-offset-2 col-sm-3 col-sm-offset-1 col-xs-12">
					  			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo noMargin">31<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6  col-md-6 col-sm-6 col-xs-6 ">
					  				<h3 class="font-pt-regular text-gd font-subheader">Alto</h3>
					  			</div>
					  		</div>
							<!-- MEDIO -->
					  		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
					  			<div class="col-lg-6  col-md-6 col-sm-6 col-xs-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo noMargin">63<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6  col-md-6 col-sm-6 col-xs-6">
					  				<h3 class="font-pt-regular text-gd font-subheader">Medio</h3>
					  			</div>
					  		</div>
					  		<!-- Bajo -->
					  		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
					  			<div class="col-lg-6  col-md-6 col-sm-6 col-xs-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo noMargin">6<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6  col-md-6 col-sm-6 col-xs-6">
					  				<h3 class="font-pt-regular text-gd font-subheader">Bajo</h3>
					  			</div>
					  		</div>
					  	</div>
					  
					  <!-- NIVEL DE ESTUDIOS -->
					  	<div class="col-lg-12 bg-white paddingAll50 overflow study-box">
					  		<h3 class="font-pt-regular text-gd font-title paddingLeft50 marginBottom20">Nivel de Estudios</h3>
					  		<!-- UNIVERSITARIO -->
					  		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-header noMargin">33<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-title">Universitarios Incompleto</h3>
					  			</div>
					  		</div>
							<!-- TERCIARIO -->
					  		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-header noMargin">19<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-title">tiene estudios Terciarios</h3>
					  			</div>
					  		</div>
					  		<!-- SECUNDARIO -->
					  		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-header noMargin">25<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-title">tiene estudios Secundarios</h3>
					  			</div>
					  		</div>
					  		<!-- NS/NC -->
					  		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
					  			<div class="col-lg-6 text-CENTER">
					  				<h1 class="font-pt-bold font-header noMargin">23<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-title">Universitarios Completo/Posgrado</h3>
					  			</div>
					  		</div>
					  	</div>
					 <!-- EDAD -->
					  	<div class="col-lg-12 bg-white age-box overflow">
					  		<h3 class="font-pt-regular text-gd font-title paddingLeft50 margintop60 marginBottom20 ">Edad</h3>
					  		<!-- MENOR 24 -->
					  		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo noMargin">6<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-subheader">Menor de <br>24 años</h3>
					  			</div>
					  		</div>
							<!-- 25 Y 34 -->
					  		<div class="col-lg-4  col-md-4 col-sm-4 col-xs-4">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo noMargin">23<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-subheader">Entre <br>25 y 34 años</h3>
					  			</div>
					  		</div>
					  		<!-- 34 y 44 -->
					  		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo  noMargin">23<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-subheader">Entre <br>35 y 44 años</h3>
					  			</div>
					  		</div>
				  		</div>
				  		<div class="col-lg-12 bg-white paddingAll50 age-box overflow">
					  		<!-- 45 y 54-->
					  		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo noMargin">25<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-subheader">Entre <br>45 y 54 años</h3>
					  			</div>
					  		</div>
					  		<!-- 55 y 64 -->
					  		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo noMargin">23<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-subheader">Más de<br> 55 años</h3>
					  			</div>
					  		</div>
					  		<!-- Mas de 65 
					  		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo noMargin">00<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-subheader">Más de <br>65 años</h3>
					  			</div>
					  		</div>-->
					  	</div>
				  </div>	

				  <!-- ESTILO-->
				  <div class="tab-pane fade" id="estilo">
					  	<!-- GENERO-->
					  	<div class="col-lg-12 bg-white paddingBottom50 overflow gender-box">
					  		<h3 class="font-pt-regular text-gd font-title paddingLeft50 margintop60 marginBottom20">Género</h3>
					  		<!-- MUJER -->
					  		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					  			<div class="col-lg-3 col-lg-offset-6 col-md-3 col-md-offset-6 col-sm-5 col-sm-offset-2 col-xs-6 text-right">
					  				<img src="images/mujer.png" alt="Mujer" class="img-responsive">
					  			</div>
					  			<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
						  			<h1 class="font-pt-bold font-subjumbo">54<span class="font-span">%</span></h1>
						  			<h3 class="font-pt-bold text-g font-subheader text-left ">mujeres</h3>
					  			</div>
					  		</div>
					  		<!-- HOMBRE -->
					  		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					  			<div class="col-lg-3 col-md-3  col-sm-4 col-xs-6 text-right">
					  				<img src="images/hombre.png" alt="Hombre" class="img-responsive">
					  			</div>
					  			<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
						  			<h1 class="font-pt-bold font-subjumbo">46<span class="font-span">%</span></h1>
						  			<h3 class="font-pt-bold text-g font-subheader text-left">hombres</h3>
					  			</div>
					  		</div>
					  	</div>
					  	<!-- NSE -->
					  	<div class="col-lg-12 bg-grayl paddingAll50 overflow nse-box">
					  		<h3 class="font-pt-regular text-gd font-title paddingLeft50">Nivel Socio Económico</h3>
					  		<!-- ALTO -->
					  		<div class="col-lg-3 col-lg-offset-2 col-md-3 col-md-offset-2 col-sm-3 col-sm-offset-1 col-xs-12">
					  			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo noMargin">32<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6  col-md-6 col-sm-6 col-xs-6 ">
					  				<h3 class="font-pt-regular text-gd font-subheader">Alto</h3>
					  			</div>
					  		</div>
							<!-- MEDIO -->
					  		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
					  			<div class="col-lg-6  col-md-6 col-sm-6 col-xs-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo noMargin">64<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6  col-md-6 col-sm-6 col-xs-6">
					  				<h3 class="font-pt-regular text-gd font-subheader">Medio</h3>
					  			</div>
					  		</div>
					  		<!-- Bajo -->
					  		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
					  			<div class="col-lg-6  col-md-6 col-sm-6 col-xs-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo noMargin">4<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6  col-md-6 col-sm-6 col-xs-6">
					  				<h3 class="font-pt-regular text-gd font-subheader">Bajo</h3>
					  			</div>
					  		</div>
					  	</div>
					  
					  <!-- NIVEL DE ESTUDIOS -->
					  	<div class="col-lg-12 bg-white paddingAll50 overflow study-box">
					  		<h3 class="font-pt-regular text-gd font-title paddingLeft50 marginBottom20">Nivel de Estudios</h3>
					  		<!-- UNIVERSITARIO -->
					  		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-header noMargin">34<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-title">Universitarios Incompletos</h3>
					  			</div>
					  		</div>
					  		<!-- TERCIARIO -->
					  		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-header noMargin">19<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-title">tiene estudios Terciarios</h3>
					  			</div>
					  		</div>
					  		<!-- SECUNDARIO -->
					  		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-header noMargin">21<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-title">tiene estudios Secundarios</h3>
					  			</div>
					  		</div>
					  		<!-- NS/NC -->
					  		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
					  			<div class="col-lg-6 text-CENTER">
					  				<h1 class="font-pt-bold font-header noMargin">26<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-title">Universitarios Completo/Posgrado</h3>
					  			</div>
					  		</div>
					  	</div>
					 <!-- EDAD -->
					  	<div class="col-lg-12 bg-white age-box overflow">
					  		<h3 class="font-pt-regular text-gd font-title paddingLeft50 margintop60 marginBottom20 ">Edad</h3>
					  		<!-- MENOR 24 -->
					  		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo noMargin">9<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-subheader">Menor de <br>24 años</h3>
					  			</div>
					  		</div>
							<!-- 25 Y 34 -->
					  		<div class="col-lg-4  col-md-4 col-sm-4 col-xs-4">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo noMargin">25<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-subheader">Entre <br>25 y 34 años</h3>
					  			</div>
					  		</div>
					  		<!-- 34 y 44 -->
					  		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo  noMargin">21<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-subheader">Entre <br>35 y 44 años</h3>
					  			</div>
					  		</div>
				  		</div>
				  		<div class="col-lg-12 bg-white paddingAll50 age-box overflow">
					  		<!-- 45 y 54-->
					  		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo noMargin">22<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-subheader">Entre <br>45 y 54 años</h3>
					  			</div>
					  		</div>
					  		<!-- 55 y 64 -->
					  		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo noMargin">23<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-subheader">Más de<br> 55 años</h3>
					  			</div>
					  		</div>
					  		<!-- Mas de 65
					  		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					  			<div class="col-lg-6 text-right">
					  				<h1 class="font-pt-bold font-subjumbo noMargin">00<span class="font-span">%</span></h1>
					  			</div>
					  			<div class="col-lg-6">
					  				<h3 class="font-pt-regular text-gd font-subheader">Más de <br>65 años</h3>
					  			</div>
					  		</div>-->
					  	</div>
				  </div>


				</div>
			</div>
			
		</div>
		<!-- FUENTE DALESSIO-->
		<div class="row">
			<div class="col-lg-3 text-center col-centered center-block paddingAll50 marginTop20">		
				<span class="font-pt-regular paddingAll30">FUENTE</span>
				<img src="images/logodalessio.png" alt="ConScore">
			</div>
		</div>
		<!-- CALL TO ACTION -->
		<div class="row bg-blue">
			<div class="col-lg-3 col-centered center-block text-center call-to-action">
				<h1 class="font-pt-bold text-white">FORMATOS</h1>
				<a href="formatos_tradicionales.php"><button type="button" class="btn btn-default font-pt-bold text-blue font-xregular">Conocer</button></a>
			</div>
		</div>
	<!-- FOOTER -->
	<footer>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bg-graym">
			<div class=" col-md-6 col-xs-12 text-center">
				<img src="images/losandes_footer.png" alt="Los Andes" class="img-responsive" style="display: inline-block;">
			</div>
			
			<div class="col-lg-2 col-lg-offset-0 col-md-10 col-md-offset-1 col-sm-8 col-sm-offset-2 col-xs-12 text-blue font-pt-bold">
				<h5>Mediakit Los Andes</h5>
				<ul>
				    <li><a href="index.php">Métricas</a></li>
				    <li><a href="formatos_tradicionales.php">Formatos</a></li>
				    <li><a href="contacto.php">Contacto</a></li>
				</ul>
			</div>
		</div>
	</footer>

 	<!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- BOOSTRAP JS -->
    <script src="js/bootstrap.min.js"></script>
    
</body>
</html>