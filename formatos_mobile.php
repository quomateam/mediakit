<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Los Andes Mediakit</title>

	<!--BOOTSTRAP-->
	 <link href="css/bootstrap.min.css" rel="stylesheet">

	<!--MEDIAKIT CSS-->
	<link href="css/mediakit_css/styles.css" rel="stylesheet">

	<!-- GOOGLE FONTS-->
		<!-- MADA -->
	<link href="https://fonts.googleapis.com/css?family=Mada:400,600,700" rel="stylesheet">

	<!-- FAVICON -->
	<link rel="icon" type="image/png" href="images/favicon.ico">	
	
</head>
<body class="bg-gray">
	<!-- NAVBAR -->
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	  <div class="container">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="index.php"><img src="images/losandes.png" alt="Los Andes"></a>
	    </div>
	    <!-- Collect the nav links, forms, and other content for toggling -->
	      <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li><a href="index.php">Inicio</a></li>
		    <li class="dropdown">
	          <a href="formatos_tradicionales.php" class="dropdown-toggle" data-toggle="dropdown">Formatos<span class="caret"></span></a>
	          <ul class="dropdown-menu bg-blue" role="menu">
	            <li><a href="formatos_tradicionales.php">Tradicionales</a></li>
			    <li><a href="formatos_publinota.php">Publinota</a></li>
			    <li><a href="formatos_social.php">Mailing y Social Media</a></li>
			    <li><a href="formatos_mobile.php">Mobile</a></li>
				<li><a href="formatos_app.php">App's</a></li>
	          </ul>
	        </li>
	        <li><a href="contacto.php">Contacto</a></li>
	      </ul>	     
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container -->
	</nav>

	
	<div class="container-fluid">
		<div class="row">
			<!-- SUBNAVBAR FORMATOS -->
			<nav class="navbar navbar-format hidden-xs" role="navigation">
			  <div class="container">
			    <div class="collapse navbar-collapse navbar-right">
			      <ul class="nav nav-justified navbar-nav">
			       <li><a href="formatos_tradicionales.php">Tradicionales</a></li>
				    <li><a href="formatos_publinota.php">Publinota</a></li>
				    <li><a href="formatos_social.php">Mailing y Social Media</a></li>
				    <li><a href="formatos_mobile.php">Mobile</a></li>
				    <li><a href="formatos_app.php">APP</a></li>

			      </ul>
			    </div><!-- /.navbar-collapse -->
			  </div><!-- /.container -->
			</nav>
		</div>
	
		<div class="row">
			<!-- SIDEBAR -->
			<div class="col-sm-3 col-md-2 sidebar">
				<!-- TITULO FORMATO EN XS -->
				<!--<h3 class="font-pt-bold bg-blue text-white text-center paddingAll30 visible-xs-block">Titulo Formato</h3>-->
				<ul class="nav nav-tabs nav-sidebar nav-stacked " role="tablist">
				  <li class="active"><a href="#t1" role="tab" data-toggle="tab">Cabecera</a></li>
				  <li><a href="#t2" role="tab" data-toggle="tab">Box</a></li>
				  <li><a href="#t3" role="tab" data-toggle="tab">Zócalo</a></li>

				</ul>
	        </div>
	        <!-- CONTENIDO -->
	        <div class="col-sm-9 col-md-10 bg-white format-content overflow">
				<!-- PANELES TAB -->
				<div class="tab-content overflow">
				<!-- PANEL T1 -->
				  <div class="tab-pane fade in active" id="t1">
						<div class="col-lg-12">
							<!-- DESCRIPCION FORMATO -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 marginTop60 ">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<!-- TITULO -->
									<h1 class="font-pt-bold font-subjumbo text-blue marginBottom20">Cabecera Mobile</h1>
									<!-- SUBTITULO -->
									<h3 class="font-pt-regular font-subheader marginBottom60">300x100 px./320x100 px.</h3>
								</div>
								<!-- TIPOS DE FORMATOS -->							
								<div class="col-lg-12  col-md-12 col-sm-12 col-xs-12 marginBottom20 text-center">
									<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
										<p class="bg-grayd paddingAll10 noMargin text-white border-bottom">GIF</p>
									</div>
									<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
										<p class="bg-grayd paddingAll10 noMargin text-white border-bottom">JPG</p>
									</div>
									<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
										<p class="bg-grayd paddingAll10 noMargin text-white border-bottom">PNG</p>
									</div>
									<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 ">
										<p class="bg-grayd paddingAll10 noMargin text-white border-bottom">MAX 200kb</p>
									</div>
								</div>
								<!-- DESCRIPCION FORMATO -->
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
									<p class="font-pt-regular font-title text-gd marginBottom20">El anuncio aparece en la cabecera de la sección en tamaños Mobile.</p>									
									<!-- MARCA SPONSOR -->
									<!--<div class="col-lg-3 col-md-5 col-sm-3 col-xs-8 marginBottom40 ">
										<img src="images/iablogo.jpg" alt="IAB" class="img-responsive">
									</div>-->
								</div>
								<!-- BTN ESPECIFICACIONES -->
								<div class="col-lg-8 col-lg-offset-0 col-md-8 col-md-offset-0 col-sm-8 col-sm-offset-2 col-xs-12 marginTop60">
									<button type="button" class="btn btn-default font-pt-bold text-white font-xregular bg-blue marginBottom60" data-toggle="modal" data-target="#specifications">ESPECIFICACIONES TÉCNICAS</button>
								</div>

							</div>
							<!-- IMAGEN FORMATO -->
							<div class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-12 col-xs-12">
								<img src="images/formatos/mobile_cabecera.jpg" alt="Formato" class="img-responsive marginBottom20">
							</div>
						</div>
				  </div>
				  <!-- PANEL T2 -->
				  <div class="tab-pane fade" id="t2">
						<div class="col-lg-12">
							<!-- DESCRIPCION FORMATO -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 marginTop60 ">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<!-- TITULO -->
									<h1 class="font-pt-bold font-subjumbo text-blue marginBottom20">Box Mobile</h1>
									<!-- SUBTITULO -->
									<h3 class="font-pt-regular font-subheader marginBottom60">300x250 px.</h3>
								</div>
								<!-- TIPOS DE FORMATOS -->							
								<div class="col-lg-12  col-md-12 col-sm-12 col-xs-12 marginBottom20 text-center">
									<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
										<p class="bg-grayd paddingAll10 noMargin text-white border-bottom">GIF</p>
									</div>
									<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
										<p class="bg-grayd paddingAll10 noMargin text-white border-bottom">JPG</p>
									</div>
									<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
										<p class="bg-grayd paddingAll10 noMargin text-white border-bottom">PNG</p>
									</div>
									<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 ">
										<p class="bg-grayd paddingAll10 noMargin text-white border-bottom">MAX 200kb</p>
									</div>								
								</div>
								<!-- DESCRIPCION FORMATO -->
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
									<p class="font-pt-regular font-title text-gd marginBottom20">Este formato serepite en tamaños Mobile, cada dos o tres boxes de noticias.</p>									
									<!-- MARCA SPONSOR -->
									<!--<div class="col-lg-3 col-md-5 col-sm-3 col-xs-8 marginBottom40 ">
										<img src="images/iablogo.jpg" alt="IAB" class="img-responsive">
									</div>-->
								</div>
								<!-- BTN ESPECIFICACIONES -->
								<div class="col-lg-8 col-lg-offset-0 col-md-8 col-md-offset-0 col-sm-8 col-sm-offset-2 col-xs-12 marginTop60">
									<button type="button" class="btn btn-default font-pt-bold text-white font-xregular bg-blue marginBottom60" data-toggle="modal" data-target="#specifications">ESPECIFICACIONES TÉCNICAS</button>
								</div>

							</div>
							<!-- IMAGEN FORMATO -->
							<div class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-12 col-xs-12">
								<img src="images/formatos/mobile_box.jpg" alt="Formato" class="img-responsive marginBottom20">
							</div>
						</div>
				  </div>
				  <!-- PANEL T3 -->
				  <div class="tab-pane fade" id="t3">
						<div class="col-lg-12">
							<!-- DESCRIPCION FORMATO -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 marginTop60 ">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<!-- TITULO -->
									<h1 class="font-pt-bold font-subjumbo text-blue marginBottom20">Zócalo</h1>
									<!-- SUBTITULO -->
									<h3 class="font-pt-regular font-subheader marginBottom60">300x100 px./ 320x100 px.</h3>
								</div>
								<!-- TIPOS DE FORMATOS -->							
								<div class="col-lg-12  col-md-12 col-sm-12 col-xs-12 marginBottom20 text-center">
									<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
										<p class="bg-grayd paddingAll10 noMargin text-white border-bottom">GIF</p>
									</div>
									<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
										<p class="bg-grayd paddingAll10 noMargin text-white border-bottom">JPG</p>
									</div>
									<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
										<p class="bg-grayd paddingAll10 noMargin text-white border-bottom">PNG</p>
									</div>
									<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 ">
										<p class="bg-grayd paddingAll10 noMargin text-white border-bottom">MAX 200kb</p>
									</div>			
								</div>
								<!-- DESCRIPCION FORMATO -->
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
									<p class="font-pt-regular font-title text-gd marginBottom20">Este formato permance estático y visible permanentemente, en la base del viewport del sitio.</p>									
									<!-- MARCA SPONSOR -->
									<!--<div class="col-lg-3 col-md-5 col-sm-3 col-xs-8 marginBottom40 ">
										<img src="images/iablogo.jpg" alt="IAB" class="img-responsive">
									</div>-->
								</div>
								<!-- BTN ESPECIFICACIONES -->
								<div class="col-lg-8 col-lg-offset-0 col-md-8 col-md-offset-0 col-sm-8 col-sm-offset-2 col-xs-12 marginTop60">
									<button type="button" class="btn btn-default font-pt-bold text-white font-xregular bg-blue marginBottom60" data-toggle="modal" data-target="#specifications">ESPECIFICACIONES TÉCNICAS</button>
								</div>

							</div>
							<!-- IMAGEN FORMATO -->
							<div class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-12 col-xs-12">
								<img src="images/formatos/mobile_zocalo.jpg" alt="Formato" class="img-responsive marginBottom20">
								
							</div>
						</div>
				  </div>
				</div>

	        </div>	
		</div>
	</div>
	<!-- FOOTER -->
	<footer>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bg-graym">
			<div class=" col-md-6 col-xs-12 text-center">
				<img src="images/losandes_footer.png" alt="Los Andes" class="img-responsive" style="display: inline-block;">
			</div>
			
			<div class="col-lg-2 col-lg-offset-0 col-md-10 col-md-offset-1 col-sm-8 col-sm-offset-2 col-xs-12 text-blue font-pt-bold">
				<h5>Mediakit Los Andes</h5>
				<ul>
				    <li><a href="index.php">Métricas</a></li>
				    <li><a href="formatos_tradicionales.php">Formatos</a></li>
				    <li><a href="contacto.php">Contacto</a></li>
				</ul>
			</div>
		</div>
	</footer>
	<!-- MODAL ESPECIFICACIONES TECNICAS -->
	<div class="modal fade bs-example-modal-lg" id="specifications" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      <div class="modal-header bg-gray ">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4 class="modal-title font-pt-bold text-blue font-subheader" id="myModalLabel">Especificaciones Técnicas</h4>
	      </div>
	      <div class="modal-body overflow">
	      	<div class="col-lg-6">
	      		<!-- ESPECIFICACIONES ENTREGA DE BANNERS -->
		        <div class="col-lg-12">
		        	<h4 class="font-pt-bold text-title text-gd">Especificaciones técnicas para entrega de Banners</h4>
		        	<p class="text-blue">¿Cómo y en que términos debe entregarse los banners?</p>
					<p>La entrega de banners de Formatos Tradicionales, se deben
					entregar 24 hs antes de su publicación para controlar que no sea
					necesario hacer cambios. Una vez aprobados pasan a ser programados.</p>
					<p>La entrega de banners de Formatos Novedosos, Rich Media y Rising
					Stars IAB deben ser entregados 48 hs antes.</p>
		        </div>
		        <!-- LISTA ITEMS ENTREGA DE BANNERS -->		        
		        <div class="col-lg-12 ">
		        	<p class="text-blue">La entrega consta de:</p>
		        	<ol>
		        		<li>Archivo .Fla original (hasta version flash cs5).</li>
		        		<li>Archivo .Swf</li>
		        		<li>Archivo backup en JPG o GIF igual formato y peso del swf para dispositivos que no reproducen flash player (Fundamental para mobile).</li>
		        		<li>Block de notas en el cuál debe venir la URL de destino para poder redireccionar la campaña publicitaria.</li>
		        		<li>Los archivos .Swf deben tener las tipografías pasadas a curvas.</li>
		        	</ol>
					<p class="oblique">Consultar por el peso máximo permitido para cada formato. Los archivos gif pueden ser animados o estáticos y al igual que los archivos jpg no deben superar los 200Kb.</p>
					<p class="oblique">Para el caso de los avisos Rich Media los archivos deben ser formato swf y tampoco deben superar los 200kb, excepto aquellos que contengan videos, en cuyo caso no deben superar los 2Mb.</p>
		        </div>
		        <!-- ESPECIFICACIONES RICHMEDIA -->
		        <div class="col-lg-12 ">
		        	<h4 class="font-pt-bold text-title text-gd">Especificaciones para formatos Richmedia</h4>
		        	<p class="text-blue">VIDEOBANNERS</p>
					<p>Para conocer las especificaciones y poder descargar ejemplos en Flash sugerimos que ingresen al portal de nuestro adserver: <a href="http://www.e-planning.net/gallery/specs/videobanner.pdf" target="_blank">AQUI</a></p>
		        </div>
		         <div class="col-lg-12">
		        	<p class="text-blue">BANNERS PUSH o EXPANDIBLES</p>
					<p>Para conocer las especificaciones y poder descargar ejemplos en Flash sugerimos que ingresen al portal de nuestro adserver: <a href="http://www.e-planning.net/gallery/specs/es/banner_exp_rollover.pdf" target="_blank">AQUI</a></p>
		        </div>
		        <!-- AVISO -->
		        <div class="col-lg-12 ">
		        	<h4 class="font-pt-bold text-title" style="color:red;">Avisos</h4>
					<p>No se harán excepciones en la implementación y programación de banners RICHMEDIA si el material se envía fuera del lapso de tiempo estipulado que figura en las condiciones que quedan establecidas.</p>
					<p>Tanto Videobanners como Expandibles, una vez recibido y controlado, se envía nuestro ad-server quienes lo reprograman y reenvían para su implementación online, es por esta razón, rogamos el cumplimiento de la normativa de entrega.</p>
					<p>Ante cualquier consulta puede comunicarse con nuestro moderador enviando un correo a: <a href="mailto:traficodigital@losandes.com.ar" target="_blank">traficodigital@losandes.com.ar</a> ó <a href="mailto:traficodigital@gmail.com" target="_blank">traficodigital@gmail.com</a> de 9 a 20.00 hs.</p>
		        </div>
		        <div class="col-lg-12 ">
		        	<h4 class="font-pt-bold text-title">Inclusión de video dentro de los formatos</h4>
					<p>En cualquier formato se pude hacer la inclusión de un video. Este no debe estar embebido dentro del flash sino que se debe marcar el área donde debe ir ubicado, el área debe ocupar un sólo frame. Por favor siga las especificaciones sobre "como enviar un video".</p>
		        </div>
		        <div class="col-lg-12 ">
		        	<h4 class="font-pt-bold text-title">Duración de los videos dentro de cada formato</h4>
					<p>No hay limitaciones específicas, sin embargo se recomienda una duración de entre 7 y 10 segundos con un máximo de 30 segundos para que sea visto en su totalidad por los usuarios.</p>
		        </div>
	        </div>
	        <div class="col-lg-6">
	      		<!-- ESPECIFICACIONES ENTREGA DE BANNERS -->
		        <div class="col-lg-12">
		        	<h4 class="font-pt-bold text-title text-gd">Ayuda</h4>
		        	<p class="text-blue">Implementación de clickTag en Flash</p>
					<p>Para poder contabilizar, además de las impresiones, los clics en los banners Flash, éstos deben llevar un botón. Para hacer el botón click tag, sólo debes seguir un par de pasos muy sencillos:</p>					
					<ol>
						<li>Crea una capa nueva, y colócala primera.</li>
						<li>En el primer fotograma, crea un rectángulo de la medida del banner total.</li>
						<li> Una vez creado el rectángulo, aprieta F8, se abrirá una ventana para convertir el objeto, selecciona la opción-botón.</li>
						<li>Clickéa 2 veces sobre el botón creado, entrarás a las instancias del mismo</li>						<li></li>
						<li>Arrástra el fotograma, de la primer instancia reposo a la última zona. De esta manera habrás convertido el botón, en botón invisible!</li>
						<li>Sal del botón, haciendo doble click, y al volver al escenario selecciona el botón clickeándolo (no desde la línea de tiempo) y apriéta F9</li>
						<li>Abrirás el campo de acciones, donde deberás colocar el código que dejamos a continuación: <code>on (release){ getURL(_root.clickTag, "_blank");}</code></li>
						<li>El botón click tag ya está listo!</li>
					</ol>
		        </div>
		        <div class="col-lg-12 ">
		        	<h4 class="font-pt-bold text-title">Otras Posibilidades</h4>
		        	<p class="text-blue">¿Puedo redireccionar mi banner a un correo?</p>
					<p>Otra opción útil para fidelizar datos puntuales, por ejemplo, medir si mi publicidad dá resultado es que, al clickear el banner directamente se habrá Outlook Express y se pueda enviar un mail a un correo deseado. Para esto, el diseñador deberá colocar en el campo de acciones del botón invisible el siguiente código, en lugar del código click tag: <code>on (release) { getURL("mailto:MAIL");}</code></p>
		        </div>
		         <div class="col-lg-12 ">
		        	<p class="text-blue">Atención</p>
					<p>Donde dice MAIL usted debe colocar el e-mail al cuál desea redireccionar el anuncio.</p>
		        </div>
		        <div class="col-lg-12 ">
		        	<h4 class="font-pt-bold text-title">Medidas de las Piezas</h4>
					<p>Las medidas de las piezas no son fijas ni obligatorias, en los ejemplos se presentan opciones standars que pueden variar según cada necesidad.</p>
					<p class="text-blue">Expandible rollover vs pushdown</p>
					<p>El pushdown empuja el contenido del sitio al expandirse, en cambio el rollover se expande por encima del contenido dejando oculto lo que tiene debajo.</p>
					<p class="text-blue">Posibilidades de cambios en los formatos</p>
					<p>La mayoría de los formatos pueden implementarse a gusto de cada Publisher, los ejemplos presentados sugieren una propuesta estándar pero no obligatoria.</p>
		        </div>	  

	        </div>
	      </div>
	      <div class="modal-footer bg-gray">
	        <button type="button" class="btn btn-default font-pt-bold text-white font-xregular bg-blue" data-dismiss="modal">Cerrar</button>
			<a href="download/especificaciones_novedosos.pdf" target="_blank"><button type="button" class="btn btn-default font-pt-bold text-white font-xregular bg-blue">Descargar</button></a>
	      </div>
	    </div>
	  </div>
	</div>
	

 	<!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- BOOSTRAP JS -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>