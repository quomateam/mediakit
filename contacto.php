<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Los Andes Mediakit</title>

	<!--BOOTSTRAP-->
	 <link href="css/bootstrap.min.css" rel="stylesheet">

	<!--MEDIAKIT CSS-->
	<link href="css/mediakit_css/styles.css" rel="stylesheet">

	<!-- GOOGLE FONTS-->
		<!-- MADA -->
	<link href="https://fonts.googleapis.com/css?family=Mada:400,600,700" rel="stylesheet">

	<!-- FAVICON -->
	<link rel="icon" type="image/png" href="images/favicon.ico">	
</head>
<body class="bg-gray">
	<!-- NAVBAR -->
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	  <div class="container">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="index.php"><img src="images/losandes.png" alt="Los Andes"></a>
	    </div>
	    <!-- Collect the nav links, forms, and other content for toggling -->
	      <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li><a href="index.php">Inicio</a></li>
		    <li class="dropdown">
	          <a href="formatos_tradicionales.php" class="dropdown-toggle" data-toggle="dropdown">Formatos<span class="caret"></span></a>
	          <ul class="dropdown-menu bg-blue" role="menu">
	            <li><a href="formatos_tradicionales.php">Tradicionales</a></li>
			    <li><a href="formatos_publinota.php">Publinota</a></li>
			    <li><a href="formatos_social.php">Mailing y Social Media</a></li>
			    <li><a href="formatos_mobile.php">Mobile</a></li>
				<li><a href="formatos_app.php">App's</a></li>
	          </ul>
	        </li>
	        <li><a href="contacto.php">Contacto</a></li>
	      </ul>	     
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container -->
	</nav>

	
	<div class="container-fluid">
		<div class="row contact-content">
			<div class="col-lg-12 marginBottom60">
				<div class="col-lg-4 col-lg-offset-3 col-md-4 col-md-offset-2 col-sm-6 col-xs-12 marginBottom40">
					<!-- TITULO -->
					<h1 class="font-pt-bold font-header text-blue marginBottom20 marginTop60">Contacto</h1>
					<p class="font-pt-regular font-title marginBottom40">Si queres ponerte en contacto con la oficina de pulicidad digital del Diario Los Andes, escribinos.</p>
					<!-- FORM -->
					<form role="form">
						<div class="form-group">
	                        <label for="ContactForm_name" class="required">Nombre <span class="required">*</span></label>
	                        <input class="form-control " placeholder="Nombre" name="ContactForm[name]" id="ContactForm_name" type="text">                        
	                        <div class="errorMessage" id="ContactForm_name_em_" style="display:none"></div>
	                    </div>
	                    <div class="form-group error">
	                        <label class="required" for="ContactForm_email">Email <span class="required">*</span></label>
	                        <input class="form-control" placeholder="Email" name="ContactForm[email]" id="ContactForm_email" type="text">                        
	                        <div class="errorMessage" id="ContactForm_email_em_" style="">Email no puede ser nulo.</div>                    
	                    </div>
	                    <div class="form-group">
	                        <label for="ContactForm_body" class="required">Consulta <span class="required">*</span></label>
	                        <textarea class="form-control" placeholder="Consulta" name="ContactForm[body]" id="ContactForm_body"></textarea>
	                        <div class="errorMessage" id="ContactForm_body_em_" style="">Consulta no puede ser nulo.</div>
	                    </div>
					  <button type="submit" class="btn btn-default font-pt-bold text-white font-xregular bg-blue marginBottom60">Enviar</button>
					</form>
				</div>
				<!-- SIDEBAR DERECHA -->
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 contact">
					<!-- LOGO LOS ANDES -->
					<div class="col-lg-6 col-lg-offset-1 col-sm-10 col-sm-offset-0 col-xs-8 col-xs-offset-2 marginBottom40 marginTop60">	
						<img src="images/losandes.png" alt="" class="img-responsive">
					</div>
					<div class="clearfix"></div>
					<!-- CONTACTO EJECUTIVA DE CUENTAS -->
					<div class="col-lg-6 col-lg-offset-1 marginBottom60 ">
						<h3 class="font-pt-bold text-blue font-subheader marginBottom20">Responsable Comercial de Medios Digitales</h3>
						<h4 class="font-pt-bold text-gray font-title marginBottom40">Sabrina Salvi</h4>
						<p>Email: <br><a href="mailto:ssalvi@losandes.com.ar">ssalvi@losandes.com.ar</a></p>
						<p>Móvil: <br><a href="callto:2613042246">(+54-261) 153042246</a></p>
						<p>Teléfono: <br><a href="callto:0261491264">(+54-261) 4491264</a></p>
					</div>
					<div class="clearfix marginBottom40"></div>
				</div>
			</div>
		</div>		
	</div>
	
	<!-- FOOTER -->
	<footer>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bg-graym">
			<div class=" col-md-6 col-xs-12 text-center">
				<img src="images/losandes_footer.png" alt="Los Andes" class="img-responsive" style="display: inline-block;">
			</div>
			
			<div class="col-lg-2 col-lg-offset-0 col-md-10 col-md-offset-1 col-sm-8 col-sm-offset-2 col-xs-12 text-blue font-pt-bold">
				<h5>Mediakit Los Andes</h5>
				<ul>
				    <li><a href="index.php">Métricas</a></li>
				    <li><a href="formatos_tradicionales.php">Formatos</a></li>
				    <li><a href="contacto.php">Contacto</a></li>
				</ul>
			</div>
		</div>
	</footer>

 	<!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- BOOSTRAP JS -->
    <script src="js/bootstrap.min.js"></script>
    
</body>
</html>