<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Los Andes Mediakit</title>

	<!--BOOTSTRAP-->
	 <link href="css/bootstrap.min.css" rel="stylesheet">

	<!--MEDIAKIT CSS-->
	<link href="css/mediakit_css/styles.css" rel="stylesheet">

	<!-- GOOGLE FONTS-->
		<!-- MADA -->
	<link href="https://fonts.googleapis.com/css?family=Mada:400,600,700" rel="stylesheet">

	<!-- FAVICON -->
	<link rel="icon" type="image/png" href="images/favicon.ico">	
	
</head>
<body class="bg-gray">
	<!-- NAVBAR -->
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	  <div class="container">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="index.php"><img src="images/losandes.png" alt="Los Andes"></a>
	    </div>
	    <!-- Collect the nav links, forms, and other content for toggling -->
	     <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li><a href="index.php">Inicio</a></li>
		    <li class="dropdown">
	          <a href="formatos_tradicionales.php" class="dropdown-toggle" data-toggle="dropdown">Formatos<span class="caret"></span></a>
	          <ul class="dropdown-menu bg-blue" role="menu">
	            <li><a href="formatos_tradicionales.php">Tradicionales</a></li>
			    <li><a href="formatos_publinota.php">Publinota</a></li>
			    <li><a href="formatos_social.php">Mailing y Social Media</a></li>
			    <li><a href="formatos_mobile.php">Mobile</a></li>
				<li><a href="formatos_app.php">App's</a></li>
	          </ul>
	        </li>
	        <li><a href="contacto.php">Contacto</a></li>
	      </ul>	     
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container -->
	</nav>

	
	<div class="container-fluid">
		<div class="row">
			<!-- SUBNAVBAR FORMATOS -->
			<nav class="navbar navbar-format hidden-xs" role="navigation">
			  <div class="container">
			    <div class="collapse navbar-collapse navbar-right">
			      <ul class="nav nav-justified navbar-nav">
			       <li><a href="formatos_tradicionales.php">Tradicionales</a></li>
				    <li><a href="formatos_publinota.php">Publinota</a></li>
				    <li><a href="formatos_social.php">Mailing y Social Media</a></li>
				    <li><a href="formatos_mobile.php">Mobile</a></li>
				    <li><a href="formatos_app.php">APP</a></li>

			      </ul>
			    </div><!-- /.navbar-collapse -->
			  </div><!-- /.container -->
			</nav>
		</div>
	
		<div class="row">
			<!-- SIDEBAR -->
			<div class="col-sm-3 col-md-2 sidebar">
				<!-- TITULO FORMATO EN XS -->
				<!--<h3 class="font-pt-bold bg-blue text-white text-center paddingAll30 visible-xs-block">Titulo Formato</h3>-->
				<ul class="nav nav-tabs nav-sidebar nav-stacked " role="tablist">
				  <li class="active"><a href="#t1" role="tab" data-toggle="tab">Mailing</a></li>
				  <li><a href="#t2" role="tab" data-toggle="tab">Redes Sociales</a></li>
				</ul>
	        </div>
	        <!-- CONTENIDO -->
	        <div class="col-sm-9 col-md-10 bg-white format-content overflow">
				<!-- PANELES TAB -->
				<div class="tab-content overflow">
				  <!-- PANEL T1 -->
				  <div class="tab-pane fade in active" id="t1">
						<div class="col-lg-12">
							<!-- DESCRIPCION FORMATO -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  marginTop60 ">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<!-- TITULO -->
									<h1 class="font-pt-bold font-subjumbo text-blue marginBottom20">Mailing</h1>
									<!-- SUBTITULO -->
									<h3 class="font-pt-regular font-subheader marginBottom60"></h3>
								</div>
								<!-- DESCRIPCION FORMATO -->
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
									<p class="font-pt-regular font-title text-gd">Contamos con una base de datos actualizada de más de 25.000 usuarios registrados. Los envios se pueden segmentar de acuerdo a los siguientes criterios:</p>							
									<p class="font-pt-regular font-title text-gd marginBottom20">
										<ul>
											<li class="font-pt-regular font-title text-gd ">Zona geográfica</li>
											<li class="font-pt-regular font-title text-gd ">Edad</li>
											<li class="font-pt-regular font-title text-gd ">Sexo</li>
										</ul>
									</p>		
								</div>
								<!-- BTN ESPECIFICACIONES -->
								<div class="col-lg-8 col-lg-offset-0 col-md-8 col-md-offset-0 col-sm-8 col-sm-offset-2 col-xs-12 marginTop60">
									<button type="button" class="btn btn-default font-pt-bold text-white font-xregular bg-blue marginBottom60" data-toggle="modal" data-target="#specifications">ESPECIFICACIONES TÉCNICAS</button>
								</div>
							</div>
							<!-- IMAGEN FORMATO -->
							<div class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-12 col-xs-12">
								<img src="images/formatos/mailing.png" alt="Formato" class="img-responsive marginBottom20">
							</div>
						</div>
				  </div>
				  <!-- PANEL T2 -->
				  <div class="tab-pane fade" id="t2">
						<div class="col-lg-12">
							<!-- DESCRIPCION FORMATO -->
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  marginTop60 ">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<!-- TITULO -->
									<h1 class="font-pt-bold font-subjumbo text-blue marginBottom20">Redes Sociales</h1>
									<!-- SUBTITULO -->
									<h3 class="font-pt-regular font-subheader marginBottom60"></h3>
								</div>
								<!-- DESCRIPCION FORMATO -->
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
									<p class="font-pt-regular font-title text-gd marginBottom20">Contamos con posteos y acciones especiales en nuestras redes sociales. <a href="contacto.php" target="_blank"> Consultar acciones.</a></p>							
									<a href="https://www.facebook.com/LosAndesDiario" target="_blank"><button type="button" class="btn btn-default font-pt-bold text-white font-regular bg-blue marginBottom20">FACEBOOK</button></a>
									<a href="https://twitter.com/LosAndesDiario" target="_blank"><button type="button" class="btn btn-default font-pt-bold text-white font-regular bg-blue marginBottom20">TWITTER</button></a>
								</div>
									
								<!-- METRICAS -->
								<div class="col-lg-12 paddingAll50 study-box">
							  		<!-- FACEBOOK -->
							  		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
							  			<div class="col-lg-12 text-center">
							  				<h1 class="font-pt-bold font-subheader marginBottom20 ">+800 mil</h1>
							  			</div>
							  			<div class="col-lg-12">
							  				<h3 class="font-pt-bold text-gd font-title text-center"><img src="images/facebook.png"></h3>
							  			</div>
							  		</div>
									<!-- TWITTER -->
							  		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
							  			<div class="col-lg-12 text-center">
							  				<h1 class="font-pt-bold font-subheader marginBottom20 ">+185 mil</h1>
							  			</div>
							  			<div class="col-lg-12">
							  				<h3 class="font-pt-bold text-gd font-title text-center"><img src="images/twitter.png"></h3>
							  			</div>
							  		</div>
							  		<!-- INSTAGRAM -->
							  		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
							  			<div class="col-lg-12 text-center">
							  				<h1 class="font-pt-bold font-subheader marginBottom20 ">+35 mil</h1>
							  			</div>
							  			<div class="col-lg-12">
							  				<h3 class="font-pt-bold text-gd text-center font-title"><img src="images/instagram.png"></h3>
							  			</div>
							  		</div>
							  	</div>
							</div>
							<!-- IMAGEN FORMATO -->
							<div class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-12 col-xs-12">
								<img src="images/formatos/redes.png" alt="Formato" class="img-responsive marginBottom20">
							</div>
						</div>
				  </div>
				</div>

	        </div>	
		</div>
	</div>

	<!-- FOOTER -->
	<footer>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bg-graym">
			<div class=" col-md-6 col-xs-12 text-center">
				<img src="images/losandes_footer.png" alt="Los Andes" class="img-responsive" style="display: inline-block;">
			</div>
			
			<div class="col-lg-2 col-lg-offset-0 col-md-10 col-md-offset-1 col-sm-8 col-sm-offset-2 col-xs-12 text-blue font-pt-bold">
				<h5>Mediakit Los Andes</h5>
				<ul>
				    <li><a href="index.php">Métricas</a></li>
				    <li><a href="formatos_tradicionales.php">Formatos</a></li>
				    <li><a href="contacto.php">Contacto</a></li>
				</ul>
			</div>
		</div>
	</footer>
	<!-- MODAL ESPECIFICACIONES TECNICAS -->
	<div class="modal fade bs-example-modal-lg" id="specifications" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      <div class="modal-header bg-gray ">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4 class="modal-title font-pt-bold text-blue font-subheader" id="myModalLabel">Especificaciones Técnicas</h4>
	      </div>
	      <div class="modal-body overflow">
	      	<div class="col-lg-12">
	      		<!-- ESPECIFICACIONES ENTREGA DE BANNERS -->
		        <div class="col-lg-12 bg-white ">
		        	<h4 class="font-pt-bold text-title text-gd">Especificaciones técnicas para envio de Mails</h4>
		        	<p class="text-title font-pt-bold marginTop20" style="color:red;">IMPORTANTE: Enviar el asunto del mail + fecha en que desean enviarlo.</p>
					<p>- Enviar adjunto el html con las imágenes en jpg o gif en calidad web.</p>
					<p>- Ancho máximo del html: 600 pixeles.</p>
					<p>- Peso máximo de la suma de las imágenes: 60kb.</p>
					<p>- Sin flash ni objetos Javascript.</p>
					<p>- Si lleva estilos deben estar in-line y no llamando a un archivo externo .css</p>
					<p>- Si lleva links deben venir en el html original.</p>
		        </div>
	        </div>
	        
	      </div>
	      <div class="modal-footer bg-white">
	        <button type="button" class="btn btn-default font-pt-bold text-white font-xregular bg-blue" data-dismiss="modal">Cerrar</button>
	      </div>
	    </div>
	  </div>
	</div>
	

 	<!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- BOOSTRAP JS -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>